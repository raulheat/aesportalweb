// abrir dropdown
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Cerrar dropdown
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}


// tabs
function showtab(tabs)
{
	var tab=tabs;
	switch(tab)
    {
      case "tab-1":
        document.getElementById('tab-container').innerHTML = document.getElementById("tab-1").innerHTML;
        document.getElementById("tab-1").style.display= "none";
        break;
      case "tab-2":
		document.getElementById('tab-container').innerHTML = document.getElementById("tab-2").innerHTML;
        document.getElementById("tab-1").style.display= "none";
        break;
      case "tab-3":
		document.getElementById('tab-container').innerHTML = document.getElementById("tab-3").innerHTML;
      document.getElementById("tab-1").style.display= "none";
        break;
      case "tab-4":
		document.getElementById('tab-container').innerHTML = document.getElementById("tab-4").innerHTML;
      document.getElementById("tab-1").style.display= "none";
        break;
      case "tab-5":
		document.getElementById('tab-container').innerHTML = document.getElementById("tab-5").innerHTML;
      document.getElementById("tab-1").style.display= "none";
        break;
      default:
		document.getElementById('tab-container').innerHTML = document.getElementById("tab-1").innerHTML;
        break;
    }
}

// contador
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
